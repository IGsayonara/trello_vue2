import Vue from "vue";
import App from "./App.vue";
import Vuex from "vuex";
import store from "./store";
import vClickOutside from "v-click-outside";
import VModal from "vue-js-modal";

Vue.config.productionTip = false;
Vue.use(vClickOutside);
Vue.use(VModal);

new Vue({
  store,
  render: (h) => h(App),
}).$mount("#app");
