import Vue from "vue";

export default {
  state: {
    cards: [
      { title: "Na na na", order: 0, id: 0, colId: 0, descr: "" },
      { title: "Yead daddy", order: 1, id: 1, colId: 0, descr: "" },
      { title: "Nope", order: 2, id: 2, colId: 0, descr: "" },
      { title: "Fack the police", order: 3, id: 3, colId: 0, descr: "" },
    ],
    newId: 4,
    editingCardId: -1,
  },

  getters: {
    getAllCards(state) {
      return [...state.cards].sort((a, b) => a.order - b.order);
    },
    getEditingCard(state) {
      return state.cards.find((el) => el.id === state.editingCardId);
    },
  },
  actions: {
    cardMoved(ctx, e) {
      ctx.commit("cardMoved", e);
    },
    cardAdded(ctx, obj) {
      ctx.commit("cardAdded", obj);
    },
    cardRemoved(ctx, obj) {
      ctx.commit("cardRemoved", obj);
    },
    addCard(ctx, obj) {
      ctx.commit("addCard", obj);
    },
    cardToEdit(ctx, id) {
      ctx.commit("cardToEdit", id);
    },
    editCardTitle(ctx, title) {
      ctx.commit("editCardTitle", title);
    },
    editCardDescr(ctx, descr) {
      ctx.commit("editCardDescr", descr);
    },
    deleteCard(ctx) {
      ctx.commit("deleteCard");
    },
    deleteCardsInColumn(ctx, colId) {
      ctx.commit("deleteCardsInColumn", colId);
    },
  },

  mutations: {
    cardMoved(state, e) {
      let thisColId = e.moved.element.colId;
      let oldIndex = e.moved.oldIndex;
      let newIndex = e.moved.newIndex;
      let toUp = newIndex < oldIndex;
      if (toUp) {
        state.cards
          .filter((el) => {
            return el.colId === thisColId;
          })
          .filter((el) => el.order >= newIndex && el.order <= oldIndex)
          .forEach((el) => {
            el.order++;
          });
      } else {
        state.cards
          .filter((el) => {
            return el.colId === thisColId;
          })
          .filter((el) => el.order >= oldIndex && el.order <= newIndex)
          .forEach((el) => {
            el.order--;
          });
      }
      e.moved.element.order = e.moved.newIndex;
    },
    cardAdded(state, obj) {
      let thisColId = obj.colId;
      let newIndex = obj.e.added.newIndex;
      obj.e.added.element.colId = obj.colId;
      state.cards
        .filter((el) => el.colId === thisColId)
        .filter((el) => el.order >= newIndex)
        .forEach((el) => {
          el.order++;
        });
      obj.e.added.element.order = newIndex;
    },
    cardRemoved(state, obj) {
      console.log(obj);
      let thisColId = obj.colId;
      let oldIndex = obj.e.removed.oldIndex;
      state.cards
        .filter((el) => el.colId === thisColId)
        .filter((el) => el.order >= oldIndex)
        .forEach((el) => {
          el.order--;
        });
    },
    addCard(state, obj) {
      let order = state.cards.filter((el) => el.colId === obj.colId).length;
      state.cards.push({
        title: obj.cardTitle,
        id: state.newId++,
        order: order,
        colId: obj.colId,
        descr: "",
      });
    },
    cardToEdit(state, id) {
      state.editingCardId = id;
    },
    editCardTitle(state, title) {
      state.cards.find((el) => el.id === state.editingCardId).title = title;
    },
    editCardDescr(state, descr) {
      state.cards.find((el) => el.id === state.editingCardId).descr = descr;
    },
    deleteCard(state) {
      let index = state.cards.findIndex(
        (el) =>
          el.id === state.cards.find((el) => el.id === state.editingCardId).id
      );
      state.editingCardId = -1;
      state.cards.splice(index, 1);
    },
    deleteCardsInColumn(state, colId) {
      state.cards = state.cards.filter((el) => el.colId !== colId);
    },
  },
};
