import Vue from "vue";

export default {
  state: {
    columns: [
      { title: "Col1", id: 0, order: 0 },
      { title: "Col2", id: 1, order: 1 },
      { title: "Col3", id: 2, order: 2 },
    ],
    newId: 3,
  },

  getters: {
    getAllColumns(state) {
      return [...state.columns].sort((a, b) => a.order - b.order);
    },
  },
  actions: {
    columnMoved(ctx, e) {
      ctx.commit("columnMoved", e);
    },
    addColumn(ctx, title) {
      ctx.commit("addColumn", title);
    },
    columnTitleEdit(ctx, obj) {
      ctx.commit("columnTitleEdit", obj);
    },
    deleteColumn(ctx, id) {
      ctx.commit("deleteColumn", id);
    },
  },

  mutations: {
    columnMoved(state, e) {
      let oldIndex = e.moved.oldIndex;
      let newIndex = e.moved.newIndex;
      let toUp = newIndex < oldIndex;
      if (toUp) {
        state.columns
          .filter((el) => el.order >= newIndex && el.order <= oldIndex)
          .forEach((el) => {
            el.order++;
          });
      } else {
        state.columns
          .filter((el) => el.order >= oldIndex && el.order <= newIndex)
          .forEach((el) => {
            el.order--;
          });
      }
      e.moved.element.order = e.moved.newIndex;
    },
    addColumn(state, title) {
      state.columns.push({
        title: title,
        id: state.newId++,
        order: state.columns.length,
      });
    },
    columnTitleEdit(state, obj) {
      state.columns.find((el) => el.id === obj.id).title = obj.title;
    },
    deleteColumn(state, id) {
      let index = state.columns.findIndex((el) => el.id === id);
      state.columns.splice(index, 1);
    },
  },
};
